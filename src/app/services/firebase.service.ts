import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase'
import {Router} from "@angular/router";
import {log} from "util";
import {forEach} from "@angular/router/src/utils/collection";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  boolArray = [];

  constructor(public db: AngularFirestore,
              private router: Router) {
  }

  getAvatars() {
    return this.db.collection('/avatar').valueChanges()
  }

  getUser(userKey) {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      return this.db.collection(`/${currentUserEmail}`).doc(userKey).snapshotChanges();
    }
  }

  updateUser(userKey, value) {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      value.nameToSearch = value.name.toLowerCase();
      value.command = this.boolControl(value.command)
      return this.db.collection(`/${currentUserEmail}`).doc(userKey).set(value);
    }
  }

  deleteUser(userKey) {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      return this.db.collection(`/${currentUserEmail}`).doc(userKey).delete();
    }
  }


  getUsers() {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      return this.db.collection(`/${currentUserEmail}`).snapshotChanges();
    }
  }


  searchUsers(searchValue) {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      return this.db.collection(`/${currentUserEmail}`, ref => ref.where('nameToSearch', '>=', searchValue)
        .where('nameToSearch', '<=', searchValue + '\uf8ff'))
        .snapshotChanges()
    }
  }

  searchUsersByAge(value) {
    return this.db.collection('users', ref => ref.orderBy('age').startAt(value)).snapshotChanges();
  }

  boolControl(num) {
    if (num === '1') {
      return this.boolArray = [true, false, false]
    }
    if (num === '2') {
      return this.boolArray = [true, true, false]
    }
    if (num === '3') {
      return this.boolArray = [true, true, true]
    }
    if (num === '') {
      return this.boolArray = [false, false, false]
    }
  }

  createUser(value, avatar, wealth, damage) {
    if (firebase.auth().currentUser.email !== null) {
      const currentUserEmail = firebase.auth().currentUser.email;
      return this.db.collection(`${currentUserEmail}`).add({
        game_master: value.game_master,
        campaign: value.campaign,

        name: value.name,
        race: value.race,
        nameToSearch: value.name.toLowerCase(),
        profession: value.profession,
        previous_profession: value.previous_profession,
        gender: value.gender,
        age: parseInt(value.age),
        height: parseInt(value.height),
        weight: parseInt(value.weight),
        eyes: value.eyes,
        hair: value.hair,
        star_sign: value.star_sign,
        marks: value.marks,

        birthplace: value.birthplace,
        family: (value.family),
        social_status: value.social_status,
        past: value.past,
        motivation: value.motivation,
        serves: value.serves,
        friends: value.friends,
        foes: value.foes,
        likes: value.likes,
        dislikes: value.dislikes,
        personality: value.personality,
        goals: value.goals,

        wealth: wealth,
        gold: value.gold,
        silver: value.silver,
        bronze: value.bronze,

        melee: [value.melee1, value.melee2, value.melee3],
        range: [value.range1, value.range2, value.range3],
        strength: [value.strength1, value.strength2, value.strength3],
        toughness: [value.toughness1, value.toughness2, value.toughness3],
        agility: [value.agility1, value.agility2, value.agility3],
        intelligence: [value.intelligence1, value.intelligence2, value.intelligence3],
        will_power: [value.will_power1, value.will_power2, value.will_power3],
        fellowship: [value.fellowship1, value.fellowship2, value.fellowship3],
        attacks: [value.attacks1, value.attacks2, value.attacks3],
        wounds: [value.wounds1, value.wounds2, value.wounds3],
        strength_bonus: [Math.floor(value.strength3 / 10)],
        toughness_bonus: [Math.floor(value.toughness3 / 10)],
        movement: [value.movement1, value.movement2, value.movement3],
        magic: [value.magic1, value.magic2, value.magic3],
        instability: [value.instability1, value.instability2, value.instability3],
        fortune: [value.fortune1, value.fortune2, value.fortune3],

        command: this.boolControl(value.command),
        gamble: this.boolControl(value.gamble),
        ride: this.boolControl(value.ride),
        drinking: this.boolControl(value.drinking),
        animals: this.boolControl(value.animals),
        gossip: this.boolControl(value.gossip),
        swim: this.boolControl(value.swim),
        drive: this.boolControl(value.drive),
        charm: this.boolControl(value.charm),
        search: this.boolControl(value.search),
        creep: this.boolControl(value.creep),
        perception: this.boolControl(value.perception),
        survival: this.boolControl(value.survival),
        haggle: this.boolControl(value.haggle),
        hiding: this.boolControl(value.hiding),
        rowing: this.boolControl(value.rowing),
        climbing: this.boolControl(value.climbing),
        evaluate: this.boolControl(value.evaluate),
        intimidation: this.boolControl(value.intimidation),
        knowledge: this.boolControl(value.knowledge),
        language: this.boolControl(value.language),

        damage: damage,

        weapon1: [value.weapon1Name,value.weapon1Dmg, value.weapon1MinRange, value.weapon1MaxRange, value.weapon1Overload,value.weapon1Quality],
        weapon2: [value.weapon2Name,value.weapon2Dmg, value.weapon2MinRange, value.weapon2MaxRange, value.weapon2Overload,value.weapon2Quality],
        weapon3: [value.weapon3Name,value.weapon3Dmg, value.weapon3MinRange, value.weapon3MaxRange, value.weapon3Overload,value.weapon3Quality],
        weapon4: [value.weapon4Name,value.weapon4Dmg, value.weapon4MinRange, value.weapon4MaxRange, value.weapon4Overload,value.weapon4Quality],
        weapon5: [value.weapon5Name,value.weapon5Dmg, value.weapon5MinRange, value.weapon5MaxRange, value.weapon5Overload,value.weapon5Quality],

        armor1: [value.armor1Name, value.armor1Credit],
        armor2: [value.armor2Name, value.armor2Credit],
        armor3: [value.armor3Name, value.armor3Credit],
        armor4: [value.armor4Name, value.armor4Credit],
        armor5: [value.armor5Name, value.armor5Credit],
        avatar: avatar,

      });
    }
  }
}
