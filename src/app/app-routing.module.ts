import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomepageComponent} from "./components/homepage/homepage.component";
import {CardsListComponent} from "./components/cards-list/cards-list.component";
import {AddCartComponent} from "./components/add-cart/add-cart.component";
import {CardDetailsComponent} from "./components/card-details/card-details.component";

const routes: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'login', redirectTo: 'home', pathMatch: 'full'},
  {path: 'cardsList', component: CardsListComponent},
  {path: 'cardsList/:id', component: CardDetailsComponent},
  {path: 'addCart', component: AddCartComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
