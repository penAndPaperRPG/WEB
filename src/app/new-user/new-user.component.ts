import {Component, OnInit, ViewChild, ViewChildren} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AvatarDialogComponent } from "../avatar-dialog/avatar-dialog.component";
import { Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import {MatTableDataSource, MatPaginator} from "@angular/material";


export interface Wealth {
  name: string;
  action: string;
}
export interface Damage {
  name: string;
  action: string;
}


@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  @ViewChild('uploadResultPaginatorWealth', {read: MatPaginator}) uploadResultPaginatorWealth: MatPaginator;
  @ViewChild('uploadResultPaginatorDamage', {read: MatPaginator}) uploadResultPaginatorDamage: MatPaginator;




  panelOpenState = false;
  exampleForm: FormGroup;
  wealth = [];
  damage = []
  avatarLink = 'https://www.wprost.pl/_thumb/41/3b/317dd1e252c4a44571b606ce330b.jpeg';

  //table
  displayedColumns: string[] = ['name','action'];
  dataSource = new MatTableDataSource(this.wealth);
  damageData = new MatTableDataSource(this.damage);


  validation_messages = {
   'name': [
     { type: 'required', message: 'Name is required.' }
   ],
   'race': [
     { type: 'required', message: 'Race is required.' }
   ],
   'age': [
     { type: 'required', message: 'Age is required.' },
   ]
 };
  private $scope: any;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit() {
    this.createForm();
    this.dataSource.paginator = this.uploadResultPaginatorWealth;
    this.damageData.paginator = this.uploadResultPaginatorDamage;
}

  createForm() {
    this.exampleForm = this.fb.group({

      game_master: [''],
      campaign: [''],
      name: ['', Validators.required ],
      race: [''],
      profession: [''],
      previous_profession: [''],
      gender: [''],
      age: [''],
      height: [''],
      weight: [''],
      eyes: [''],
      hair: [''],
      star_sign: [''],
      marks: [''],
      birthplace: [''],
      family: [''],
      social_status: [''],
      past: [''],
      motivation: [''],
      serves: [''],
      friends: [''],
      foes: [''],
      likes: [''],
      dislikes: [''],
      personality: [''],
      goals: [''],
      gold: [''],
      silver: [''],
      bronze: [''],

      melee1: [''],
      melee2: [''],
      melee3: [''],
      range1: [''],
      range2: [''],
      range3: [''],
      strength1: [''],
      strength2: [''],
      strength3: [''],
      toughness1: [''],
      toughness2: [''],
      toughness3: [''],
      agility1: [''],
      agility2: [''],
      agility3: [''],
      intelligence1: [''],
      intelligence2: [''],
      intelligence3: [''],
      will_power1: [''],
      will_power2: [''],
      will_power3: [''],
      fellowship1: [''],
      fellowship2: [''],
      fellowship3: [''],
      attacks1: [''],
      attacks2: [''],
      attacks3: [''],
      wounds1: [''],
      wounds2: [''],
      wounds3: [''],
      movement1: [''],
      movement2: [''],
      movement3: [''],
      magic1: [''],
      magic2: [''],
      magic3: [''],
      instability1: [''],
      instability2: [''],
      instability3: [''],
      fortune1: [''],
      fortune2: [''],
      fortune3: [''],
      command: [''],
      gamble: [''],
      ride: [''],
      drinking: [''],
      animals: [''],
      gossip: [''],
      swim: [''],
      drive: [''],
      charm: [''],
      search: [''],
      creep: [''],
      perception: [''],
      survival: [''],
      haggle: [''],
      hiding: [''],
      rowing: [''],
      climbing: [''],
      evaluate: [''],
      intimidation : [''],
      knowledge : [''],
      language : [''],

      weapon1Name : [''],
      weapon1Dmg : [''],
      weapon1MinRange : [''],
      weapon1MaxRange : [''],
      weapon1Overload : [''],
      weapon1Quality : [''],

      weapon2Name : [''],
      weapon2Dmg : [''],
      weapon2MinRange : [''],
      weapon2MaxRange : [''],
      weapon2Overload : [''],
      weapon2Quality : [''],

      weapon3Name : [''],
      weapon3Dmg : [''],
      weapon3MinRange : [''],
      weapon3MaxRange : [''],
      weapon3Overload : [''],
      weapon3Quality : [''],

      weapon4Name : [''],
      weapon4Dmg : [''],
      weapon4MinRange : [''],
      weapon4MaxRange : [''],
      weapon4Overload : [''],
      weapon4Quality : [''],

      weapon5Name : [''],
      weapon5Dmg : [''],
      weapon5MinRange : [''],
      weapon5MaxRange : [''],
      weapon5Overload : [''],
      weapon5Quality : [''],

      armor1Name : [''],
      armor1Credit : [''],
      armor2Name : [''],
      armor2Credit : [''],
      armor3Name : [''],
      armor3Credit : [''],
      armor4Name : [''],
      armor4Credit : [''],
      armor5Name : [''],
      armor5Credit : [''],

    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      height: '400px',
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.avatarLink = result.link;
      }
    });
  }

  resetFields(){
    this.exampleForm = this.fb.group({
      game_master: new FormControl('', Validators.required),
      campaign: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      race: new FormControl('', Validators.required),
      profession: new FormControl('', Validators.required),
      previous_profession: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      eyes: new FormControl('', Validators.required),
      hair: new FormControl('', Validators.required),
      star_sign: new FormControl('', Validators.required),
      marks: new FormControl('', Validators.required),
      birthplace: new FormControl('', Validators.required),
      family: new FormControl('', Validators.required),
      social_status: new FormControl('', Validators.required),
      past: new FormControl('', Validators.required),
      motivation: new FormControl('', Validators.required),
      serves: new FormControl('', Validators.required),
      friends: new FormControl('', Validators.required),
      foes: new FormControl('', Validators.required),
      likes: new FormControl('', Validators.required),
      dislikes: new FormControl('', Validators.required),
      personality: new FormControl('', Validators.required),
      goals: new FormControl('', Validators.required),
      gold: new FormControl('', Validators.required),
      silver: new FormControl('', Validators.required),
      bronze: new FormControl('', Validators.required),

      melee1: new FormControl('', Validators.required),
      melee2: new FormControl('', Validators.required),
      melee3: new FormControl('', Validators.required),
      range1: new FormControl('', Validators.required),
      range2: new FormControl('', Validators.required),
      range3: new FormControl('', Validators.required),
      strength1: new FormControl('', Validators.required),
      strength2: new FormControl('', Validators.required),
      strength3: new FormControl('', Validators.required),
      toughness1: new FormControl('', Validators.required),
      toughness2: new FormControl('', Validators.required),
      toughness3: new FormControl('', Validators.required),
      agility1: new FormControl('', Validators.required),
      agility2: new FormControl('', Validators.required),
      agility3: new FormControl('', Validators.required),
      intelligence1: new FormControl('', Validators.required),
      intelligence2: new FormControl('', Validators.required),
      intelligence3: new FormControl('', Validators.required),
      will_power1: new FormControl('', Validators.required),
      will_power2: new FormControl('', Validators.required),
      will_power3: new FormControl('', Validators.required),
      fellowship1: new FormControl('', Validators.required),
      fellowship2: new FormControl('', Validators.required),
      fellowship3: new FormControl('', Validators.required),
      attacks1: new FormControl('', Validators.required),
      attacks2: new FormControl('', Validators.required),
      attacks3: new FormControl('', Validators.required),
      wounds1: new FormControl('', Validators.required),
      wounds2: new FormControl('', Validators.required),
      wounds3: new FormControl('', Validators.required),
      movement1: new FormControl('', Validators.required),
      movement2: new FormControl('', Validators.required),
      movement3: new FormControl('', Validators.required),
      magic1: new FormControl('', Validators.required),
      magic2: new FormControl('', Validators.required),
      magic3: new FormControl('', Validators.required),
      instability1: new FormControl('', Validators.required),
      instability2: new FormControl('', Validators.required),
      instability3: new FormControl('', Validators.required),
      fortune1: new FormControl('', Validators.required),
      fortune2: new FormControl('', Validators.required),
      fortune3: new FormControl('', Validators.required),
      command: new FormControl('', Validators.required),
      gamble: new FormControl('', Validators.required),
      ride: new FormControl('', Validators.required),
      drinking: new FormControl('', Validators.required),
      animals: new FormControl('', Validators.required),
      gossip: new FormControl('', Validators.required),
      swim: new FormControl('', Validators.required),
      drive: new FormControl('', Validators.required),
      charm: new FormControl('', Validators.required),
      search: new FormControl('', Validators.required),
      creep: new FormControl('', Validators.required),
      perception: new FormControl('', Validators.required),
      survival: new FormControl('', Validators.required),
      haggle: new FormControl('', Validators.required),
      hiding: new FormControl('', Validators.required),
      rowing: new FormControl('', Validators.required),
      climbing: new FormControl('', Validators.required),
      evaluate: new FormControl('', Validators.required),
      intimidation: new FormControl('', Validators.required),
      knowledge: new FormControl('', Validators.required),
      language: new FormControl('', Validators.required),

      weapon1Name: new FormControl('', Validators.required),
      weapon1Dmg: new FormControl('', Validators.required),
      weapon1MinRange: new FormControl('', Validators.required),
      weapon1MaxRange: new FormControl('', Validators.required),
      weapon1Overload: new FormControl('', Validators.required),
      weapon1Quality: new FormControl('', Validators.required),

      weapon2Name: new FormControl('', Validators.required),
      weapon2Dmg: new FormControl('', Validators.required),
      weapon2MinRange: new FormControl('', Validators.required),
      weapon2MaxRange: new FormControl('', Validators.required),
      weapon2Overload: new FormControl('', Validators.required),
      weapon2Quality: new FormControl('', Validators.required),

      weapon3Name: new FormControl('', Validators.required),
      weapon3Dmg: new FormControl('', Validators.required),
      weapon3MinRange: new FormControl('', Validators.required),
      weapon3MaxRange: new FormControl('', Validators.required),
      weapon3Overload: new FormControl('', Validators.required),
      weapon3Quality: new FormControl('', Validators.required),

      weapon4Name: new FormControl('', Validators.required),
      weapon4Dmg: new FormControl('', Validators.required),
      weapon4MinRange: new FormControl('', Validators.required),
      weapon4MaxRange: new FormControl('', Validators.required),
      weapon4Overload: new FormControl('', Validators.required),
      weapon4Quality: new FormControl('', Validators.required),

      weapon5Name: new FormControl('', Validators.required),
      weapon5Dmg: new FormControl('', Validators.required),
      weapon5MinRange: new FormControl('', Validators.required),
      weapon5MaxRange: new FormControl('', Validators.required),
      weapon5Overload: new FormControl('', Validators.required),
      weapon5Quality: new FormControl('', Validators.required),

      armor1Name: new FormControl('', Validators.required),
      armor1Credit : new FormControl('', Validators.required),
      armor2Name: new FormControl('', Validators.required),
      armor2Credit : new FormControl('', Validators.required),
      armor3Name: new FormControl('', Validators.required),
      armor3Credit : new FormControl('', Validators.required),
      armor4Name: new FormControl('', Validators.required),
      armor4Credit : new FormControl('', Validators.required),
      armor5Name: new FormControl('', Validators.required),
      armor5Credit : new FormControl('', Validators.required),
    });
  }


  addWealth(wealthValue: string, wealthInput: HTMLInputElement){

    if(wealthValue == ''){
      console.log('empty')
    } else{
      this.wealth.push(wealthValue)
      this.dataSource._updateChangeSubscription();
    }
    wealthInput.value = ''
  }

  addDamage(damageValue: string, damageInput: HTMLInputElement){

    if(damageValue == ''){
      console.log('empty')
    } else{
      this.damage.push(damageValue)
      this.damageData._updateChangeSubscription();
    }
    damageInput.value = ''
  }

  remove(item: Wealth) : void{
    const index = this.wealth.indexOf(item);
    if(index >= 0){
      this.wealth.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.wealth);
      this.dataSource.paginator = this.uploadResultPaginatorWealth;
    }
  }
  removeDamage(item: Damage) : void{
    const index = this.damage.indexOf(item);
    if(index >= 0){
      this.damage.splice(index, 1);
      this.damageData = new MatTableDataSource(this.damage);
      this.damageData.paginator = this.uploadResultPaginatorDamage;
    }
  }

  onSubmit(value){
    this.firebaseService.createUser(value, this.avatarLink, this.wealth, this.damage)
    .then(
      res => {
        this.resetFields();
        this.router.navigate(['/cards']);
      }
    )
  }

}
