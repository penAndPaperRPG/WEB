import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {AvatarDialogComponent} from "../avatar-dialog/avatar-dialog.component";
import {FirebaseService} from '../services/firebase.service';
import {Router} from '@angular/router';
import {Wealth} from "../new-user/new-user.component";


export interface Wealth {
  name: string;
  action: string;
}

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  @ViewChild('uploadResultPaginatorWealth', {read: MatPaginator}) uploadResultPaginatorWealth: MatPaginator;


  exampleForm: FormGroup;
  item: any;
  panelOpenState = false;
  displayedColumns: string[] = ['name', 'action'];
  wealth = [];
  dataSource: any;

  command = [];
  gamble = [];

  abbControl = {
    commandW: false,
    command10: false,
    command20: false,
    gambleW: false,
    gamble10: false,
    gamble20: false,
  }


  boolControl(tab: any) {
    if (tab[0] == true && tab[1] == false && tab[2] == false) {
      this.abbControl.commandW = true
    }
    if (tab[0] == true && tab[1] == true && tab[2] == false) {
      this.abbControl.command10 = true
    }
    if (tab[0] == true && tab[1] == true && tab[2] == true) {
      this.abbControl.command20 = true;
    }
    if (tab[0] == false && tab[1] == false && tab[2] == false) {
      this.abbControl.commandW = false;
      this.abbControl.command10 = false;
      this.abbControl.command20 = false;
    }
  }


  validation_messages = {
    'name': [
      {type: 'required', message: 'Name is required.'}
    ],
    'race': [
      {type: 'required', message: 'Race is required.'}
    ],
    'age': [
      {type: 'required', message: 'Age is required.'},
    ]
  };

  constructor(
    public firebaseService: FirebaseService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data.payload.data();

        this.wealth = this.item.wealth;
        this.command = this.item.command;
        this.gamble = this.item.gamble;

        this.item.id = data.payload.id;
        this.createForm();
      }
      this.boolControl(this.command)
    });
    this.dataSource = new MatTableDataSource(this.wealth);
    this.dataSource.paginator = this.uploadResultPaginatorWealth;

  }

  createForm() {
    this.exampleForm = this.fb.group({
      name: [this.item.name, Validators.required],
      race: [this.item.race],
      age: [this.item.age],
      game_master: [this.item.game_master],
      campaign: [this.item.campaign],
      profession: [this.item.profession],
      previous_profession: [this.item.previous_profession],
      gender: [this.item.gender],
      height: [this.item.height],
      weight: [this.item.weight],
      eyes: [this.item.eyes],
      hair: [this.item.hair],
      star_sign: [this.item.star_sign],
      marks: [this.item.marks],

      birthplace: [this.item.birthplace],
      family: [this.item.family],
      social_status: [this.item.social_status],
      past: [this.item.past],
      motivation: [this.item.motivation],
      serves: [this.item.serves],
      friends: [this.item.friends],
      foes: [this.item.foes],
      likes: [this.item.likes],
      dislikes: [this.item.dislikes],
      personality: [this.item.personality],
      goals: [this.item.goals],

      wealth: [this.item.wealth],
      //stats
      melee1: [this.item.melee1],
      melee2: [this.item.melee2],
      melee3: [this.item.melee3],
      range1: [this.item.range1],
      range2: [this.item.range2],
      range3: [this.item.range3],
      strength1: [this.item.strength1],
      strength2: [this.item.strength2],
      strength3: [this.item.strength3],
      toughness1: [this.item.toughness1],
      toughness2: [this.item.toughness2],
      toughness3: [this.item.toughness3],
      agility1: [this.item.agility1],
      agility2: [this.item.agility2],
      agility3: [this.item.agility3],
      intelligence1: [this.item.intelligence1],
      intelligence2: [this.item.intelligence2],
      intelligence3: [this.item.intelligence3],
      will_power1: [this.item.will_power1],
      will_power2: [this.item.will_power2],
      will_power3: [this.item.will_power3],
      fellowship1: [this.item.fellowship1],
      fellowship2: [this.item.fellowship2],
      fellowship3: [this.item.fellowship3],
      attacks1: [this.item.attacks1],
      attacks2: [this.item.attacks2],
      attacks3: [this.item.attacks3],
      wounds1: [this.item.wounds1],
      wounds2: [this.item.wounds2],
      wounds3: [this.item.wounds3],
      strength_bonus1: [this.item.strength_bonus1],
      strength_bonus2: [this.item.strength_bonus2],
      strength_bonus3: [this.item.strength_bonus3],
      toughness_bonus1: [this.item.toughness_bonus1],
      toughness_bonus2: [this.item.toughness_bonus2],
      toughness_bonus3: [this.item.toughness_bonus3],
      movement1: [this.item.movement1],
      movement2: [this.item.movement2],
      movement3: [this.item.movement3],
      magic1: [this.item.magic1],
      magic2: [this.item.magic2],
      magic3: [this.item.magic3],
      instability1: [this.item.instability1],
      instability2: [this.item.instability2],
      instability3: [this.item.instability3],
      fortune1: [this.item.fortune1],
      fortune2: [this.item.fortune2],
      fortune3: [this.item.fortune3],
      //abb
      command: [this.item.command],
      gamble: [this.item.gamble],
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      height: '400px',
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.item.avatar = result.link;
      }
    });
  }

  onSubmit(value) {
    value.avatar = this.item.avatar;
    value.age = Number(value.age);
    this.firebaseService.updateUser(this.item.id, value)
      .then(
        res => {
          this.router.navigate(['/cards']);
        }
      )
    console.log(this.command)
  }

  delete() {
    this.firebaseService.deleteUser(this.item.id)
      .then(
        res => {
          this.router.navigate(['/cards']);
        },
        err => {
          console.log(err);
        }
      )
  }


  addWealth(wealthValue: string, wealthInput: HTMLInputElement) {

    if (wealthValue == '') {
      console.log('empty')
    } else {
      this.wealth.push(wealthValue)
      this.dataSource._updateChangeSubscription();
    }
    wealthInput.value = ''
  }

  remove(item: Wealth): void {
    const index = this.wealth.indexOf(item);
    if (index >= 0) {
      this.wealth.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.wealth);
      this.dataSource.paginator = this.uploadResultPaginatorWealth;
    }
  }

  cancel() {
    this.router.navigate(['/cards']);
  }

}
