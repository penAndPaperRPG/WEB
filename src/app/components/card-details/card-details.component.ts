import { Component, OnInit } from '@angular/core';
import {CardService} from "../../services&models/services/card/card.service";
import * as firebase from "firebase";
import {AngularFirestore} from "@angular/fire/firestore";
import {Router} from "@angular/router";

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})

export class CardDetailsComponent implements OnInit {

  constructor(private service: CardService,
              private firestore: AngularFirestore,
              private router: Router) { }


  url: string;
  urlArray: any;
  id: string;
  cardDataArray: any;
  db = firebase.firestore();



  ngOnInit() {
    this.getId();
    this.getCardData(this.id);


  }

  getId(){
    this.url = this.router.url;
    this.urlArray = this.url.split('/',3);
    return this.id = this.urlArray[2];
  }

  getCardData(id:string){
    this.db.collection('pan479p@gmail.com').doc(id).get()
      .then(snapshot => {
        this.cardDataArray = snapshot.data();
      })
  }



}

