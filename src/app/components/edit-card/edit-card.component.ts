import { Component, OnInit } from '@angular/core';
import {CardService} from "../../services&models/services/card/card.service";
import * as firebase from "firebase";
import {AngularFirestore} from "@angular/fire/firestore";
import {Router} from "@angular/router";


@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss']
})
export class EditCardComponent implements OnInit {

  constructor(private service: CardService,
              private firestore: AngularFirestore,
              private router: Router
  ) { }


  url: string;
  urlArray: any;
  id: string;
  cardDataArray: any;
  db = firebase.firestore();
  currentUser = firebase.auth().currentUser;
  currentUserEmail = this.currentUser.email;


  ngOnInit() {
    this.getId();
    this.getCardData(this.id);

  }

  getId(){
    this.url = this.router.url;
    this.urlArray = this.url.split('/',3);
    return this.id = this.urlArray[2];
  }

  getCardData(id:string){
    this.db.collection(this.currentUserEmail).doc(id).get()
      .then(snapshot => {
        this.cardDataArray = snapshot.data();
      })
  }

  /*Update(){
    this.db.collection('pan479p@gmail.com').doc(this.id).update({
      name: 'Szalomator'
    })
  }*/



}
