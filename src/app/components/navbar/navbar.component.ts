import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services&models/services/user/user.service";
import {MatDialog} from "@angular/material";
import {LoginComponent} from "../login/login.component";
import * as firebase from "firebase";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(public user: UserService,
              public dialog: MatDialog) { }


  openLoginDialog(){
    this.dialog.open(LoginComponent);
  }

  ngOnInit() {

  }

}
