import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services&models/services/user/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public user: UserService) { }

  ngOnInit() {
  }

}
