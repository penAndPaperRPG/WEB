import {Component, OnInit, ViewChild} from '@angular/core';
import {CardService} from "../../services&models/services/card/card.service";
import {NgForm} from "@angular/forms";
import {AngularFirestore} from "@angular/fire/firestore";
import {ToastrService} from "ngx-toastr";
import * as firebase from "firebase";
import {Router} from "@angular/router";
import {MatTableDataSource, MatPaginator} from "@angular/material";
import {AngularFireDatabase} from "@angular/fire/database";


export interface Wealth {
  name: string
}

@Component({
  selector: 'app-add-cart',
  templateUrl: './add-cart.component.html',
  styleUrls: ['./add-cart.component.scss']
})

export class AddCartComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  errorFormMessage = 'To pole jest puste';

  breakpointSkills: number;
  addArr = [];
  displayedColumns: string[] = ['name'];
  dataSource = new MatTableDataSource(this.addArr);


  constructor(private service: CardService,
              private firestore: AngularFirestore,
              private toastr: ToastrService,
              private router: Router,
              private db: AngularFireDatabase) {
  }

  currentUser = firebase.auth().currentUser;
  currentUserEmail = this.currentUser.email;
  arrayStats = [];

  featuredPhotoSelected(event: any){
    const randomId = Math.random().toString(36).substring(2);
    const file: File = event.target.files[0];
    const metaData = {'contentType': file.type};
    const storageRef: firebase.storage.Reference = firebase.storage().ref(`photos/${this.currentUserEmail}/${randomId}`);
    storageRef.put(file, metaData);


  }


  ngOnInit() {
    this.resetForm();
    this.dataSource.paginator = this.paginator;

    this.breakpointSkills = (window.innerWidth <= 780) ? 1 : 3
  }

  addWealth(wealthValue: string) {
    if (wealthValue == '') {
    } else {
      this.service.formData.wealth = []
      this.addArr.push(wealthValue)
      this.dataSource._updateChangeSubscription()
    }
  }

  remove(item: Wealth): void {
    const index = this.addArr.indexOf(item);
    if (index >= 0) {
      this.addArr.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.addArr)
      this.dataSource.paginator = this.paginator;
    }
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      character_image: '',
      id: null,
      name: '',
      race: '',
      profession: '',
      previous_profession: '',
      gender: '',
      age: null,
      height: null,
      weight: null,
      eyes: '',
      hair: '',
      star_sign: '',
      marks: '',

      game_master: '',
      campaign: '',

      birthplace: '',
      family: '',
      social_status: '',
      past: '',
      motivation: '',
      serves: '',
      friends: '',
      foes: '',
      likes: '',
      dislikes: '',
      personality: '',
      goals: '',
      wealth: [],
      gold: null,
      silver: null,
      bronze: null,
      skip: {
        melee: {
          start: null,
          diagram: null,
          currently: null
        },
        range: {
          start: null,
          diagram: null,
          currently: null
        },
        strength: {
          start: null,
          diagram: null,
          currently: null,
        },
        toughness: {
          start: null,
          diagram: null,
          currently: null
        },
        agility: {
          start: null,
          diagram: null,
          currently: null
        },
        intelligence: {
          start: null,
          diagram: null,
          currently: null
        },
        will_power: {
          start: null,
          diagram: null,
          currently: null
        },
        fellowship: {
          start: null,
          diagram: null,
          currently: null
        },
        attacks: {
          start: null,
          diagram: null,
          currently: null
        },
        wounds: {
          start: null,
          diagram: null,
          currently: null
        },
        strength_bonus: {
          start: null,
          diagram: null,
          currently: null
        },

        toughness_bonus: {
          start: null,
          diagram: null,
          currently: null
        },
        movement: {
          start: null,
          diagram: null,
          currently: null
        },
        magic: {
          start: null,
          diagram: null,
          currently: null
        },
        instability: {
          start: null,
          diagram: null,
          currently: null
        },
        fortune: {
          start: null,
          diagram: null,
          currently: null
        }


      },
      stats: {},
      abilities: {},
      abb: {
        command: {w: false, w10: false, w20: false, stat: ''},
        gamble: {w: false, w10: false, w20: false, stat: ''},
        ride: {w: false, w10: false, w20: false, stat: ''},
        drinking: {w: false, w10: false, w20: false, stat: ''},
        animals: {w: false, w10: false, w20: false, stat: ''},
        gossip: {w: false, w10: false, w20: false, stat: ''},
        swim: {w: false, w10: false, w20: false, stat: ''},
        drive: {w: false, w10: false, w20: false, stat: ''},
        charm: {w: false, w10: false, w20: false, stat: ''},
        search: {w: false, w10: false, w20: false, stat: ''},
        creep: {w: false, w10: false, w20: false, stat: ''},
        perception: {w: false, w10: false, w20: false, stat: ''},
        survival: {w: false, w10: false, w20: false, stat: ''},
        haggle: {w: false, w10: false, w20: false, stat: ''},
        hiding: {w: false, w10: false, w20: false, stat: ''},
        rowing: {w: false, w10: false, w20: false, stat: ''},
        climbing: {w: false, w10: false, w20: false, stat: ''},
        evaluate: {w: false, w10: false, w20: false, stat: ''},
        intimidation: {w: false, w10: false, w20: false, stat: ''},
      }
    }
  }

  assignArrayStats() {
    this.arrayStats.push({
      melee: {
        start: this.service.formData.skip.melee.start,
        diagram: this.service.formData.skip.melee.diagram,
        currently: this.service.formData.skip.melee.currently
      },
      range: {
        start: this.service.formData.skip.range.start,
        diagram: this.service.formData.skip.range.diagram,
        currently: this.service.formData.skip.range.currently
      },
      strength: {
        start: this.service.formData.skip.strength.start,
        diagram: this.service.formData.skip.strength.diagram,
        currently: this.service.formData.skip.strength.currently
      },
      toughness: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      agility: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      intelligence: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      will_power: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      fellowship: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      attacks: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      wounds: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      strength_bonus: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      toughness_bonus: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      movement: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      magic: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      instability: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      },

      fortune: {
        start: this.service.formData.skip.toughness.start,
        diagram: this.service.formData.skip.toughness.diagram,
        currently: this.service.formData.skip.toughness.currently
      }


    });
    for (let i = 0; i < this.arrayStats.length; i++) {
      this.service.formData.stats[i] = this.arrayStats[i]
    }
  }

  testArray = []

  boolControl(value: string, valueAbb: string, stat: string) {
    let status = 'ok'

    if (value === 'W') {
      this.testArray.push({
        [valueAbb]: {w: true, w10: false, w20: false, stat: stat},
      })
      status = 'nope'
    } else if (value === 'W10') {
      this.testArray.push({
        [valueAbb]: {w: true, w10: true, w20: false, stat: stat},
      })
      status = 'nope'
    } else if (value === 'W20') {
      this.testArray.push({
        [valueAbb]: {w: true, w10: true, w20: true, stat: stat},
      })
      status = 'nope'
    } else if (status !== 'nope') {
      this.testArray.push({
        [valueAbb]: {w: false, w10: false, w20: false, stat: stat}
      })
    }
  }

  saveAbb() {
    let value = this.service.formData.abb;

    this.boolControl(value.command.toString(), 'command', 'Ogd');
    this.boolControl(value.gamble.toString(), 'gamble', 'Int');
    this.boolControl(value.ride.toString(), 'ride', 'Zr');
    this.boolControl(value.drinking.toString(), 'drinking', 'Odp');
    this.boolControl(value.animals.toString(), 'animals', 'Int');
    this.boolControl(value.gossip.toString(), 'gossip', 'Ogd');
    this.boolControl(value.swim.toString(), 'swim', 'K');
    this.boolControl(value.drive.toString(), 'drive', 'K');
    this.boolControl(value.charm.toString(), 'charm', 'Ogd');
    this.boolControl(value.search.toString(), 'search', 'Int');
    this.boolControl(value.creep.toString(), 'creep', 'Zr');
    this.boolControl(value.perception.toString(), 'perception', 'Int');
    this.boolControl(value.survival.toString(), 'survival', 'Int');
    this.boolControl(value.haggle.toString(), 'haggle', 'Ogd');
    this.boolControl(value.hiding.toString(), 'hiding', 'Zr');
    this.boolControl(value.rowing.toString(), 'rowing', 'K');
    this.boolControl(value.climbing.toString(), 'climbing', 'K');
    this.boolControl(value.evaluate.toString(), 'evaluate', 'Int');
    this.boolControl(value.intimidation.toString(), 'intimidation', 'K');

    for (let i = 0; i < this.testArray.length; i++) {
      this.service.formData.abilities[i] = this.testArray[i]
    }

  }


  //RWD
  onResize(event) {
    this.breakpointSkills = (event.target.innerWidth <= 780) ? 1 : 3;
  }

  onSubmit(form: NgForm) {
    for (let i = 0; i < this.addArr.length; i++) {
      this.service.formData.wealth[i] = this.addArr[i]
    }
    this.assignArrayStats();
    this.saveAbb();
    let data = form.value;

    this.firestore.collection(`${this.currentUserEmail}`).add(data);
    this.resetForm(form);
    this.toastr
      .success('Dodano pomyślnie', 'Pen&Paper system')
    this.router.navigate(['/cardsList'])
  }

}
