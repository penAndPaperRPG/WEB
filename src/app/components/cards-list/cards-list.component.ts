import {Component, OnInit} from '@angular/core';
import {CardService} from "../../services&models/services/card/card.service";
import {CardModel} from "../../services&models/models/card/card.model";
import {AngularFirestore} from "@angular/fire/firestore";
import * as firebase from "firebase";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {

  cards: CardModel[];

  constructor(private service: CardService,
              private firestore: AngularFirestore,
              private toastr: ToastrService) {
  }


  ngOnInit() {
    this.service.getCards().subscribe(actionArray => {
      this.cards = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as CardModel
      })
    })
  }

  currentUser = firebase.auth().currentUser;
  currentUserEmail = this.currentUser.email;

  onDelete(id: string) {
    if (confirm('Jesteś pewny?')) {
      this.firestore.doc(this.currentUserEmail+'/'+id).delete();
      this.toastr.warning('Usunięto pomyślnie', 'Pen&Paper system')
    }
  }
}
