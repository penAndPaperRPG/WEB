export class CardModel {

  character_image: string;

  id: string;
  name: string;
  race: string;
  profession: string;
  previous_profession: string;
  gender: string;
  age: number;
  height: number;
  weight: number;
  eyes: string;
  hair: string;
  star_sign: string;
  marks: string;

  game_master: string;
  campaign: string;

  birthplace: string;
  family: string;
  social_status: string;
  past: string;

  motivation: string;
  serves: string;
  friends: string;
  foes: string;
  likes: string;
  dislikes: string;
  personality: string;
  goals: string;

  wealth: string[];
  gold: number;
  silver: number;
  bronze: number;

  skip: {
    melee: {
      start: number;
      diagram: number;
      currently: number
    }
    range: {
      start: number;
      diagram: number;
      currently: number
    }
    strength: {
      start: number;
      diagram: number;
      currently: number
    }
    toughness: {
      start: number;
      diagram: number;
      currently: number
    }
    agility: {
      start: number;
      diagram: number;
      currently: number
    }

    intelligence: {
      start: number;
      diagram: number;
      currently: number
    }
    will_power: {
      start: number;
      diagram: number;
      currently: number
    }
    fellowship: {
      start: number;
      diagram: number;
      currently: number
    }
    attacks: {
      start: number;
      diagram: number;
      currently: number
    }
    wounds: {
      start: number;
      diagram: number;
      currently: number
    }
    strength_bonus: {
      start: number;
      diagram: number;
      currently: number
    }
    toughness_bonus: {
      start: number;
      diagram: number;
      currently: number
    }
    movement: {
      start: number;
      diagram: number;
      currently: number
    }
    magic: {
      start: number;
      diagram: number;
      currently: number
    }
    instability: {
      start: number;
      diagram: number;
      currently: number
    }
    fortune: {
      start: number;
      diagram: number;
      currently: number
    }
  };
  stats: {};

  abb: {
    command: { w: boolean; w10: boolean; w20: boolean, stat: string }
    gamble: { w: boolean; w10: boolean; w20: boolean; stat: string }
    ride: { w: boolean; w10: boolean; w20: boolean; stat: string }
    drinking: { w: boolean; w10: boolean; w20: boolean; stat: string }
    animals: { w: boolean; w10: boolean; w20: boolean; stat: string }
    gossip: { w: boolean; w10: boolean; w20: boolean; stat: string }
    swim: { w: boolean; w10: boolean; w20: boolean; stat: string }
    drive: { w: boolean; w10: boolean; w20: boolean; stat: string }
    charm: { w: boolean; w10: boolean; w20: boolean; stat: string }
    search: { w: boolean; w10: boolean; w20: boolean; stat: string }
    creep: { w: boolean; w10: boolean; w20: boolean; stat: string }
    perception: { w: boolean; w10: boolean; w20: boolean; stat: string }
    survival: { w: boolean; w10: boolean; w20: boolean; stat: string }
    haggle: { w: boolean; w10: boolean; w20: boolean; stat: string }
    hiding: { w: boolean; w10: boolean; w20: boolean; stat: string }
    rowing: { w: boolean; w10: boolean; w20: boolean; stat: string }
    climbing: { w: boolean; w10: boolean; w20: boolean; stat: string }
    evaluate: { w: boolean; w10: boolean; w20: boolean; stat: string }
    intimidation: { w: boolean; w10: boolean; w20: boolean; stat: string }
  }
  abilities: {}


}
