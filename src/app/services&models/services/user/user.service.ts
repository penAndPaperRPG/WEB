import {Injectable} from '@angular/core';
import {Observable ,of as observableOf} from 'rxjs'
import {AngularFireAuth} from "@angular/fire/auth";
import {map, switchMap} from "rxjs/operators";
import {auth} from 'firebase';
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  uid = this.afAuth.authState.pipe(
    map(authState => {
      if (!authState) {
        return null;
      } else {
        return authState.uid;
      }
    })
  );

  email = this.afAuth.authState.pipe(
    map(authState => {
      if (!authState) {
        return null;
      } else {
        return authState.email;
      }
    })
  );

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase,
              ) {}

  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}

