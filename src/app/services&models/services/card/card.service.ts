import {Injectable} from '@angular/core';
import {CardModel} from "../../models/card/card.model";
import {AngularFirestore} from "@angular/fire/firestore";
import * as firebase from "firebase";

@Injectable({
  providedIn: 'root'
})
export class CardService {

  formData: CardModel;
  private currentUser: firebase.User;
  private currentUserEmail: string;

  constructor(private firestore: AngularFirestore) {
  }

  getCards() {
    this.currentUser = firebase.auth().currentUser;
    this.currentUserEmail = this.currentUser.email;
    return this.firestore.collection(`${this.currentUserEmail}`).snapshotChanges();
  }
}
